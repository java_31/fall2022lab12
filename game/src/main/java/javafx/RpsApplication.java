/*Kayci Nicole Davila 2141560
 * This class is used to setup the GUI and and set user interactions
 */
package javafx;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
    private RpsGame game = new RpsGame();
    
    public void start(Stage stage) {
        Group root = new Group();
        //scene is associated with container, dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.PURPLE);
        //associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        //adding buttons and textfields
        Button rock_btn = new Button("rock");
        Button paper_btn = new Button("paper");
        Button scissor_btn = new Button("scissors");

        VBox topSection = new VBox();
        HBox buttonsContainer = new HBox();
        //adding buttons to its container
        buttonsContainer.getChildren().addAll(rock_btn,paper_btn,scissor_btn);
        
        HBox textContainer = new HBox();
        TextField greeting = new TextField("Welcome!!");
        TextField wins = new TextField("Wins: "+game.getWins());
        TextField losses = new TextField("Losses: "+game.getLosses());
        TextField ties = new TextField("Ties: "+game.getTies());
        
        //adding attributes to the textfields
        greeting.setPrefWidth(250);
        wins.setPrefWidth(200);
        losses.setPrefWidth(200);
        ties.setPrefWidth(200);
        greeting.setEditable(false);
        wins.setEditable(false);
        losses.setEditable(false);
        ties.setEditable(false);

        //adding everything to the group
        textContainer.getChildren().addAll(greeting,wins,losses,ties);
        topSection.getChildren().addAll(buttonsContainer, textContainer);
        root.getChildren().addAll(topSection);
        stage.show();

        //passing values to the RpsChoice class
        RpsChoice actionRock = new RpsChoice(greeting, wins, losses, ties, "rock", game);
        RpsChoice actionPaper = new RpsChoice(greeting, wins, losses, ties, "paper", game);
        RpsChoice actionScissors = new RpsChoice(greeting, wins, losses, ties, "scissors", game);

        //setting the action to the buttons
        rock_btn.setOnAction(actionRock);
        paper_btn.setOnAction(actionPaper);
        scissor_btn.setOnAction(actionScissors);
    }


    public static void main(String[] args) {
    Application.launch(args);
    }
} 

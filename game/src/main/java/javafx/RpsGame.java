/*Kayci Nicole Davila
 * 2141560
 * This class is used to create the RpsGame object which allows a game of rock paper scissors to be played 
 */
package javafx;
import java.util.*;

public class RpsGame {
    int wins;
    int ties;
    int losses;
    Random randNum;

    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        randNum = new Random();
    }
    public int getWins() {
        return wins;
    }
    public int getTies() {
        return ties;
    }
    public int getLosses() {
        return losses;
    }

    //This method takes a User String and plays a round of rock paper scissor
    public String playRound (String choice){
        String result="";
        String [] comOptions = {"rock","paper","scissors"};
        int rand = this.randNum.nextInt(3);
        
        /*all possible wins and losses*/
        if(choice.equals(comOptions[rand])){
            this.ties++;
            result = "Computer choses "+comOptions[rand]+ " this game is a tie!";
        }
        else if(choice.equals("scissors") && comOptions[rand].equals("paper")){
            this.wins++;
            result = "Computer choses "+comOptions[rand]+" you won!";
        }
        else if(choice.equals("scissors") && comOptions[rand].equals("rock")){
            this.losses++;
            result = "Computer choses "+comOptions[rand]+" the computer won!";
        }
        else if(choice.equals("paper") && comOptions[rand].equals("rock")){
            this.wins++;
            result = "Computer choses "+comOptions[rand]+" you won!";
        }
        else if(choice.equals("paper") && comOptions[rand].equals("scissors")){
            this.losses++;
            result = "Computer choses "+comOptions[rand]+ " the computer won!";
        }
        else if(choice.equals("rock") && comOptions[rand].equals("scissors")){
            this.wins++;
            result = "Computer choses "+comOptions[rand]+" you won!";
        }
        else if(choice.equals("rock") && comOptions[rand].equals("paper")){
            this.losses++;
            result = "Computer choses "+comOptions[rand]+" the computer won!";
        }
        return result;
    }    
}

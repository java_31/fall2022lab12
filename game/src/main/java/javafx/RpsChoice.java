/*Kayci Nicole Davila 2141560
 * This class implements EventHandler to handle the event once a user interacts in order to update the GUI based on results
 */
package javafx;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
    private TextField greetings;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String choice;
    private RpsGame game;  

    public RpsChoice(TextField greetings, TextField wins, TextField losses, TextField ties, String choice, RpsGame game) {
        this.greetings = greetings;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.choice = choice;
        this.game = game;
    }
    /*overriding handle method to update output after user interacts */
    @Override
    public void handle(ActionEvent arg0) {
        String message = game.playRound(choice);
        greetings.setText(message);
        wins.setText("Wins: "+game.getWins());
        losses.setText("Losses: "+game.getLosses());
        ties.setText("Ties: "+game.getTies());
    }
    
}
